# More info:  
  
> [WIKI](https://bitbucket.org/kamaradski/t-a/wiki/Home)    
> [F.A.Q](https://bitbucket.org/kamaradski/t-a/wiki/Frequently%20Asked%20Questions)  
> [Change-Log](https://bitbucket.org/kamaradski/t-a/wiki/Change-log)  
> [Open Issues](https://bitbucket.org/kamaradski/t-a/issues?status=new&status=open)  
> [Future ideas](https://bitbucket.org/kamaradski/t-a/issues?q=TODO&status=new&status=open&status=on%20hold&status=wontfix)  
  
# Forums & releases:  
  
> [AhoyWorld Forums](http://www.ahoyworld.co.uk/topic/2709-transport-ambush-the-thread)  
> [BIS Forums](http://forums.bistudio.com/showthread.php?184558-Transport-amp-Ambush-(Altis))  
> [Armaholic](http://www.armaholic.com/page.php?id=27087)  
> [Steam Workshop](http://steamcommunity.com/sharedfiles/filedetails/?id=275140474)  
  
# Modded version:  
  
> Fork of the vanilla T&A Modded for AhoyWorld.co.uk EU#3 Server  
> This version also serves as a DEV-sandbox to try different new features before they make it into vanilla
>  
> **Minimal Modset Required:**  
> - Authentic Gameplay Modification  
> - ASDG Joint Rails  
> - Community Base addons A3  
> - TaskForce Radio  
> - RHS: Escalation  
>  
> **Sourcecode & Development:**  
> https://bitbucket.org/kamaradski/t-a-modded-eu-3/commits/all  
>  
> Any comments, code contributions or other ideas ? Let me know and I will be happy to consider to add.