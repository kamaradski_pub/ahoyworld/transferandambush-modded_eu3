// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			kmissionEnd.sqf
// Author:			wildw1ng 2013
// Contributers:	Quiksilver & kamaradski 2014
//
// Gear restriction script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#define SNIPER_MSG "Only Snipers may use this weapon system. Sniper rifle removed."

while {true} do {

	// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	// 	Restrict Sniper Rifles:
	// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

	if ((player hasWeapon "srifle_GM6_F") || (player hasWeapon "srifle_GM6_LRPS_F") || (player hasWeapon "srifle_LRR_F") || (player hasWeapon "srifle_GM6_SOS_F") || (player hasWeapon "srifle_GM6_camo_F") || (player hasWeapon "srifle_GM6_camo_SOS_F") || (player hasWeapon "srifle_GM6_camo_LRPS_F") || (player hasWeapon "srifle_LRR_camo_F") || (player hasWeapon "srifle_LRR_camo_LRPS_F") || (player hasWeapon "srifle_LRR_camo_SOS_F") || (player hasWeapon "srifle_LRR_LRPS_F") || (player hasWeapon "srifle_LRR_SOS_F") || (player hasWeapon "rhs_weap_svdp") || (player hasWeapon "rhs_weap_svdp_wd") || (player hasWeapon "rhs_weap_svds") || (player hasWeapon "rhs_weap_svdp_npz") || (player hasWeapon "rhs_weap_svdp_wd_npz") || (player hasWeapon "rhs_weap_svds_npz") || (player hasWeapon "rhs_weap_XM2010") || (player hasWeapon "rhs_weap_XM2010_wd") || (player hasWeapon "rhs_weap_XM2010_d") || (player hasWeapon "rhs_weap_XM2010_sa") || (player hasWeapon "rhs_weap_sr25") || (player hasWeapon "rhs_weap_sr25_ec")) then
	{
		if ((playerSide == west && typeOf player != "B_sniper_F") || (playerside == east && typeOf player != "O_sniper_F") || (playerSide == resistance && typeOf player != "I_Sniper_F") || (typeOf player != "I_G_sniper_F")) then
		{
			player removeWeapon (primaryWeapon player);
			titleText [SNIPER_MSG, "PLAIN", 3];
		};
	};
	
	
	sleep 5.0;
};