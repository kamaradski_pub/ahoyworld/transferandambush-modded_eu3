//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			veh_setup.sqf
// Author:			Kamaradski 2014
// Contributers:	none
//
// ServerSide RUNONCE vehicle setup script for "Transport & Ambush"
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


_DisTER = [hunter1,hunter2,hunter3,hunter4,ifrit1,ifrit2,ifrit3];
_kClear = [hunter1,hunter2,hunter3,hunter4,ifrit1,ifrit2,ifrit3,offroad1,offroad2,offroad3,offroad4,offroad5,offroad6,truck1,truck2,truck3,truck4,truck5,truck6,truck7,truck8,truck9,truck10,helo1,helo2, medevac];
_Kfuel = [hunter1,hunter2,hunter3,hunter4,ifrit1,ifrit2,ifrit3,offroad1,offroad2,offroad3,offroad4,offroad5,offroad6,truck1,truck2,truck3,truck4,truck5,truck6,truck7,truck8,truck9,truck10,helo1,armor1,helo2,armor2, medevac];
_Kevac = [medevac];

// Disable thermals
	{
		_x disableTIEquipment true;
	} foreach _DisTER;

sleep 0.2;

// Empty inventory all vehicles
	{
		[[[_x],"int_scripts\clearinv.sqf"],"BIS_fnc_execVM",nil,true] spawn BIS_fnc_MP;
	} foreach _kClear;

sleep 0.2;

// Spawn vehicles with random fuel
	{
		kINTFuelLoad=(round(random 10))/10;
		_x setFuel kINTFuelLoad;
	} foreach _Kfuel;

sleep 0.2;
	
// set custom texture & Lock cargo
	{
		_x setObjectTextureGlobal [0, "art\medivac.paa"];
		for [{_i=2}, {_i<17}, {_i=_i+1}] do {
			_x lockCargo [_i, true];
		};
	} foreach _Kevac;

diag_log format ["%1: Transport & Ambush: Runonce vehicle setup completed:", time];