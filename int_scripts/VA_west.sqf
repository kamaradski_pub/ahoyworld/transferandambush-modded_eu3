//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			VA_west.sqf
// Author:		Kamaradski 2014
// Contributers:	Razgriz33
//
// 
// Drop-in Virtual Arsenal ammo-box for "Transport & Ambush"
// _K = [this] execVM "int_scripts\VA_west.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

sleep 5; // allow other modules to load first

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Setup VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_myBox = _this select 0;

_myBox allowDamage false; 
_myBox enableSimulation false;

clearWeaponCargo _myBox;
clearMagazineCargo _myBox;
clearBackpackCargo _myBox;
clearItemCargo _myBox;

["AmmoboxInit",[_myBox,true]] call BIS_fnc_arsenal;

	
	
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Empty VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[true],true] call BIS_fnc_removeVirtualBackpackCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualItemCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualWeaponCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualMagazineCargo;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add side independent gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"V_Chestrig_oli",
	"H_Shemag_olive",
	"ItemMap",
	"ItemCompass",
	"ItemWatch",
	"ItemRadio",
	"V_BandollierB_blk",
	"H_Cap_oli",
	"V_Chestrig_blk",
	"H_Watchcap_blk",
	"V_TacVest_blk",
	"H_Booniehat_khk",
	"G_Goggles_VR",
	"H_Bandanna_khk",
	"H_Watchcap_camo",
	"V_BandollierB_khk",
	"ItemGPS"
],true] call BIS_fnc_addVirtualItemCargo;	

[_myBox,[
	"LMG_M200",
	"Rangefinder",
	"Laserdesignator",
	"arifle_SDAR_F",
	"arifle_TRG21_F",
	"arifle_TRG20_F",
	"arifle_TRG20_ACO_F",
	"hgun_ACPC2_F",
	"Binocular",
	"arifle_Mk20_GL_ACO_F",
	"LMG_Mk200_F",
	"arifle_Mk20_F",
	"arifle_Mk20C_ACO_F",
	"arifle_TRG21_GL_F",
	"arifle_Mk20_MRCO_F",
	"launch_RPG32_F",
	"arifle_TRG21_MRCO_F"
],true] call BIS_fnc_addVirtualWeaponCargo;	
	
	
[_myBox,[
	"Chemlight_blue",
	"9Rnd_45ACP_Mag",
	"SmokeShellRed",
	"SmokeShellBlue",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_Smoke_Grenade_shell",
	"1Rnd_SmokeGreen_Grenade_shell",
	"1Rnd_SmokeRed_Grenade_shell",
	"1Rnd_SmokeBlue_Grenade_shell",
	"APERSMine_Range_Mag",
	"HandGrenade",
	"MiniGrenade",
	"RPG32_F",
	"16Rnd_9x21_Mag",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_SmokeOrange_Grenade_shell",
	"SmokeShellOrange",
	"SmokeShellRed",
	"ClaymoreDirectionalMine_Remote_Mag",
	"APERSTripMine_Wire_Mag",
	"Laserbatteries",
	"6Rnd_LG_scalpel",
	"2Rnd_GBU12_LGB",
	"100Rnd_127x99_mag_Tracer_Yellow"
],true] call BIS_fnc_addVirtualMagazineCargo;



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add BLUFOR gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"B_HMG_01_weapon_F",
	"B_GMG_01_weapon_F",
	"B_HMG_01_A_weapon_F",
	"B_GMG_01_A_weapon_F",
	"B_HMG_01_high_weapon_F",
	"B_GMG_01_high_weapon_F",
	"B_HMG_01_A_high_weapon_F",
	"B_GMG_01_A_high_weapon_F",
	"B_AT_01_weapon_F",
	"B_AA_01_weapon_F",
	"B_Mortar_01_weapon_F",
	"B_AssaultPack_rgr",
	"B_BergenC_grn",
	"B_Carryall_khk",
	"B_FieldPack_khk",
	"B_Kitbag_rgr",
	"B_OutdoorPack_blu",
	"B_TacticalPack_rgr",
	"B_Parachute"
],true] call BIS_fnc_addVirtualBackpackCargo;
	
[_myBox,[
	"U_B_CombatUniform_mcam_tshirt",
	"V_PlateCarrier1_rgr",
	"H_HelmetB",
	"U_B_CombatUniform_mcam_vest",
	"U_B_HeliPilotCoveralls",
	"U_B_PilotCoveralls",
	"U_B_CombatUniform_mcam",
	"U_Rangemaster",
	"H_Cap_headphones",
	"V_Rangemaster_belt",
	"V_BandollierB_rgr",
	"H_MilCap_mcamo",
	"V_PlateCarrierGL_rgr",
	"H_HelmetSpecB_blk",
	"V_PlateCarrier2_rgr",
	"H_HelmetB_grass",
	"H_HelmetB_desert",
	"H_HelmetSpecB",
	"H_HelmetB_sand",
	"V_PlateCarrierSpec_rgr",
	"H_HelmetB_light_desert",
	"H_HelmetB_light_sand",
	"H_PilotHelmetHeli_B",
	"H_HelmetSpecB_paint1",
	"H_HelmetSpecB_paint2",
	"V_Chestrig_rgr",
	"H_HelmetCrew_B",
	"U_Competitor",
	"H_PilotHelmetFighter_B",
	"H_CrewHelmetHeli_B",
	"B_UavTerminal",
	"U_B_Wetsuit",
	"V_RebreatherB",
	"G_B_Diving",
	"H_HelmetB_plain_mcamo",
	"H_Booniehat_mcamo",
	"H_HelmetB_light",
	"G_Shades_Black",
	"U_B_GhillieSuit",
	"U_B_CTRG_2",
	"V_PlateCarrierL_CTRG",
	"V_PlateCarrier_Kerry",
	"H_Helmet_Kerry",
	"U_B_survival_uniform",
	"U_B_CTRG_1",
	"V_PlateCarrierH_CTRG",
	"H_HelmetB_light_snakeskin",
	"H_Cap_khaki_specops_UK",
	"U_B_CTRG_3",
	"U_BG_Guerilla1_1",
	"U_BG_leader",
	"U_BG_Guerilla2_1",
	"U_BG_Guerilla2_3",
	"U_BG_Guerilla2_2",
	"U_BG_Guerilla3_1",
	"U_BG_Guerrilla_6_1",
	"U_I_G_Story_Protagonist_F",
	"H_Bandanna_khk_hs",
	"U_I_G_resistanceLeader_F",
	"V_I_G_resistanceLeader_F",
	"U_B_Protagonist_VR",
	"U_BasicBody"
],true] call BIS_fnc_addVirtualItemCargo;
	
[_myBox,[
	"arifle_MX_ACO_pointer_F",
	"hgun_P07_F",
	"arifle_MX_ACO_F",
	"arifle_MX_GL_ACO_F",
	"arifle_MX_SW_pointer_F",
	"arifle_MX_Hamr_pointer_F",
	"arifle_MX_GL_Hamr_pointer_F",
	"arifle_MXM_Hamr_pointer_F",
	"arifle_MX_pointer_F",
	"arifle_MX_Holo_pointer_F",
	"arifle_MXC_Holo_pointer_F",
	"SMG_01_Holo_F",
	"arifle_MXC_F",
	"arifle_MXC_Aco_F",
	"hgun_Pistol_heavy_01_MRD_F",
	"arifle_MXC_Holo_F",
	"hgun_P07_snds_F",
	"arifle_MX_ACO_pointer_snds_F",
	"arifle_MXC_ACO_pointer_snds_F",
	"arifle_MX_RCO_pointer_snds_F",
	"srifle_EBR_DMS_pointer_snds_F",
	"arifle_MX_GL_Holo_pointer_snds_F",
	"srifle_LRR_camo_SOS_F",
	"arifle_MX_GL_Black_Hamr_pointer_F",
	"arifle_MX_Black_Hamr_pointer_F",
	"srifle_EBR_Hamr_pointer_F",
	"arifle_MX_SW_Black_Hamr_pointer_F",
	"arifle_TRG21_GL_MRCO_F"
],true] call BIS_fnc_addVirtualWeaponCargo;	
	
[_myBox,[
	"Chemlight_green",
	"B_IR_Grenade",
	"30Rnd_45ACP_Mag_SMG_01",
	"11Rnd_45ACP_Mag",
	"Chemlight_green",
	"7Rnd_408_Mag"
],true] call BIS_fnc_addVirtualMagazineCargo;

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add MODDED gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
[_myBox,[
	"AGM_Altimeter",
	"AGM_SpareBarrel",
	"AGM_NVG_Gen1",
	"AGM_NVG_Gen2",
	"AGM_NVG_Gen4",
	"AGM_NVG_Wide",
	"AGM_Box_Medical",
	"AGM_Bandage",
	"AGM_Morphine",
	"AGM_Epipen",
	"AGM_Bloodbag",
	"AGM_MapTools",
	"AGM_UAVBattery",
	"AGM_EarBuds",
	"AGM_DefusalKit",
	"AGM_CableTie",
	"AGM_IR_Strobe_Item",
	"AGM_muzzle_mzls_H",
	"AGM_muzzle_mzls_B",
	"AGM_muzzle_mzls_L",
	"AGM_muzzle_mzls_smg_01",
	"AGM_muzzle_mzls_smg_02",
	"rhsusf_ach_bare",						// USAF Headgear
	"rhsusf_ach_bare_ess",
	"rhsusf_ach_bare_headset",
	"rhsusf_ach_bare_headset_ess",
	"rhsusf_ach_bare_tan",
	"rhsusf_ach_bare_tan_ess",
	"rhsusf_ach_bare_tan_headset",
	"rhsusf_ach_bare_tan_headset_ess",
	"rhsusf_ach_bare_wood",
	"rhsusf_ach_bare_wood_ess",
	"rhsusf_ach_bare_wood_headset",
	"rhsusf_ach_bare_wood_headset_ess",
	"rhsusf_ach_bare_des",
	"rhsusf_ach_bare_des_ess",
	"rhsusf_ach_bare_des_headset",
	"rhsusf_ach_bare_des_headset_ess",
	"rhsusf_ach_bare_semi",
	"rhsusf_ach_bare_semi_ess",
	"rhsusf_ach_bare_semi_headset",
	"rhsusf_ach_bare_semi_headset_ess",
	"rhsusf_ach_helmet_ucp",
	"rhsusf_ach_helmet_ess_ucp",
	"rhsusf_ach_helmet_headset_ucp",
	"rhsusf_ach_helmet_headset_ess_ucp",
	"rhsusf_ach_helmet_ocp",
	"rhsusf_ach_helmet_camo_ocp",
	"rhsusf_ach_helmet_ess_ocp",
	"rhsusf_ach_helmet_headset_ocp",
	"rhsusf_ach_helmet_headset_ess_ocp",
	"rhsusf_ach_helmet_m81",
	"rhsusf_mich_bare",
	"rhsusf_mich_bare_headset",
	"rhsusf_mich_bare_alt",
	"rhsusf_mich_bare_norotos",
	"rhsusf_mich_bare_norotos_headset",
	"rhsusf_mich_bare_norotos_alt",
	"rhsusf_mich_bare_norotos_alt_headset",
	"rhsusf_mich_bare_norotos_arc",
	"rhsusf_mich_bare_norotos_arc_headset",
	"rhsusf_mich_bare_norotos_arc_alt",
	"rhsusf_mich_bare_norotos_arc_alt_headset",
	"rhsusf_mich_bare_tan",
	"rhsusf_mich_bare_tan_headset",
	"rhsusf_mich_bare_alt_tan",
	"rhsusf_mich_bare_norotos_tan",
	"rhsusf_mich_bare_norotos_tan_headset",
	"rhsusf_mich_bare_norotos_alt_tan",
	"rhsusf_mich_bare_norotos_alt_tan_headset",
	"rhsusf_mich_bare_norotos_arc_tan",
	"rhsusf_mich_bare_norotos_arc_tan_headset",
	"rhsusf_mich_bare_norotos_arc_alt_tan",
	"rhsusf_mich_bare_norotos_arc_alt_tan_headset",
	"rhsusf_mich_bare_semi",
	"rhsusf_mich_bare_semi_headset",
	"rhsusf_mich_bare_alt_semi",
	"rhsusf_mich_bare_norotos_semi",
	"rhsusf_mich_bare_norotos_semi_headset",
	"rhsusf_mich_bare_norotos_alt_semi",
	"rhsusf_mich_bare_norotos_alt_semi_headset",
	"rhsusf_mich_bare_norotos_arc_semi",
	"rhsusf_mich_bare_norotos_arc_semi_headset",
	"rhsusf_mich_bare_norotos_arc_alt_semi",
	"rhsusf_mich_bare_norotos_arc_alt_semi_headset",
	"rhsusf_mich_helmet_marpatwd",
	"rhsusf_mich_helmet_marpatwd_headset",
	"rhsusf_mich_helmet_marpatwd_alt",
	"rhsusf_mich_helmet_marpatwd_alt_headset",
	"rhsusf_mich_helmet_marpatwd_norotos",
	"rhsusf_mich_helmet_marpatwd_norotos_headset",
	"rhsusf_mich_helmet_marpatwd_norotos_arc",
	"rhsusf_mich_helmet_marpatwd_norotos_arc_headset",
	"rhsusf_mich_helmet_marpatd",
	"rhsusf_mich_helmet_marpatd_headset",
	"rhsusf_mich_helmet_marpatd_alt",
	"rhsusf_mich_helmet_marpatd_alt_headset",
	"rhsusf_mich_helmet_marpatd_norotos",
	"rhsusf_mich_helmet_marpatd_norotos_headset",
	"rhsusf_mich_helmet_marpatd_norotos_arc",
	"rhsusf_mich_helmet_marpatd_norotos_arc_headset",
	"rhsusf_opscore_02",
	"rhsusf_opscore_01",
	"rhsusf_opscore_02_tan",
	"rhsusf_opscore_01_tan",
	"rhsusf_opscore_04_ocp",
	"rhsusf_opscore_03_ocp",
	"rhs_Booniehat_ucp",
	"rhs_Booniehat_ocp",
	"rhs_Booniehat_marpatwd",
	"rhs_Booniehat_marpatd",
	"rhs_Booniehat_m81",
	"rhsusf_Bowman",
	"rhsusf_bowman_cap",
	"rhsusf_patrolcap_ucp",
	"rhsusf_patrolcap_ocp",
	"rhsusf_cvc_helmet",
	"rhsusf_cvc_ess",
	"rhsusf_cvc_green_helmet",
	"rhsusf_cvc_green_ess",
	"rhsusf_ANPVS_14",						// Night Vision Goggles
	"rhsusf_ANPVS_15",						// Night Vision Goggles
	"lerca_1200_black",						// Rangefinders
	"lerca_1200_tan",							// Rangefinders
	"rhs_uniform_cu_ucp",						// Army Combat Uniform
	"rhs_uniform_cu_ocp_patchless",				// Army Combat Uniform
	"rhs_uniform_cu_ocp",						// Army Combat Uniform
	"rhs_uniform_cu_ucp_patchless",				// Army Combat Uniform
	"rhs_uniform_cu_ucp_82nd",					// Army Combat Uniform
	"rhs_uniform_cu_ocp_82nd",					// Army Combat Uniform
	"rhs_uniform_cu_ucp_101st",				// Army Combat Uniform
	"rhs_uniform_cu_ocp_101st",				// Army Combat Uniform
	"rhs_uniform_cu_ucp_10th",					// Army Combat Uniform
	"rhs_uniform_cu_ocp_10th",					// Army Combat Uniform
	"rhs_uniform_FROG01_wd",					// Flame Resistant Organizational Gear
	"rhs_uniform_FROG01_d",					// Flame Resistant Organizational Gear
	"rhs_uniform_FROG01_m81",					// Flame Resistant Organizational Gear
	"rhsusf_iotv_ucp",						// USAF Vests
	"rhsusf_iotv_ucp_grenadier",				// USAF Vests
	"rhsusf_iotv_ucp_medic",					// USAF Vests
	"rhsusf_iotv_ucp_repair",					// USAF Vests
	"rhsusf_iotv_ucp_rifleman",				// USAF Vests
	"rhsusf_iotv_ucp_SAW",						// USAF Vests
	"rhsusf_iotv_ucp_squadleader",				// USAF Vests
	"rhsusf_iotv_ucp_teamleader",				// USAF Vests
	"rhsusf_iotv_ocp",						// USAF Vests
	"rhsusf_iotv_ocp_grenadier",				// USAF Vests
	"rhsusf_iotv_ocp_medic",					// USAF Vests
	"rhsusf_iotv_ocp_repair",					// USAF Vests
	"rhsusf_iotv_ocp_rifleman",				// USAF Vests
	"rhsusf_iotv_ocp_SAW",						// USAF Vests
	"rhsusf_iotv_ocp_squadleader",				// USAF Vests
	"rhsusf_iotv_ocp_teamleader",				// USAF Vests
	"rhsusf_spc",								// USAF Vests
	"rhsusf_acc_anpeq15",						// USAF Attachments
	"rhsusf_acc_anpeq15A",						// USAF Attachments
	"rhsusf_acc_anpeq15_light",				// USAF Attachments
	"rhsusf_acc_EOTECH",						// USAF Attachments
	"rhsusf_acc_eotech_552",					// USAF Attachments
	"rhsusf_acc_LEUPOLDMK4",					// USAF Attachments
	"rhsusf_acc_LEUPOLDMK4_2",					// USAF Attachments
	"rhsusf_acc_ELCAN",						// USAF Attachments
	"rhsusf_acc_ELCAN_PIP",					// USAF Attachments
	"rhsusf_acc_ACOG",						// USAF Attachments
	"rhsusf_acc_ACOG_PIP",						// USAF Attachments
	"rhsusf_acc_ACOG2",						// USAF Attachments
	"rhsusf_acc_ACOG3",						// USAF Attachments
	"rhsusf_acc_ACOG_USMC",					// USAF Attachments
	"rhsusf_acc_ACOG2_USMC",					// USAF Attachments
	"rhsusf_acc_ACOG3_USMC",					// USAF Attachments
	"rhsusf_acc_compm4"						// USAF Attachments
],true] call BIS_fnc_addVirtualItemCargo;

[_myBox,[
	"rhs_weap_fgm148",						// The FGM-148 Javelin is a U.S. fire-and-forget man portable anti-tank missile.
	"rhs_weap_fim92",							// The FIM-92F Stinger is a man-portable surface-to-air infrared-guided missile.
	"rhs_weap_M136",							// The M136 AT4 is a 84mm unguided portable single-shot anti-tank weapon.
	"rhs_weap_M136_hedp",						// The M136 AT4 is a 84mm unguided portable single-shot anti-tank weapon.
	"rhs_weap_M136_hp",						// The M136 AT4 is a 84mm unguided portable single-shot anti-tank weapon.
	"rhs_weap_m14ebrri",						// The M14 Enhanced Battle Rifle is a designated marksman evolution of the M14 rifle.
	"rhs_weap_m16a4",							// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_weap_m16a4_bipod",					// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_weap_m16a4_grip",						// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_weap_m16a4_carryhandle",				// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_weap_m16a4_carryhandle_pmag",			// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_weap_XM2010",						// The M2010 Enhanced Sniper Rifle is a derivation of the M24 sniper rifle.
	"rhs_weap_XM2010_wd",						// The M2010 Enhanced Sniper Rifle is a derivation of the M24 sniper rifle.
	"rhs_weap_XM2010_d",						// The M2010 Enhanced Sniper Rifle is a derivation of the M24 sniper rifle.
	"rhs_weap_XM2010_sa",						// The M2010 Enhanced Sniper Rifle is a derivation of the M24 sniper rifle.
	"rhs_weap_m240B",							// The M240B is a US 7.62x51mm NATO medium machine gun that is fed from disintegrating belt.
	"rhs_weap_m249_pip",						// The M249 SAW ( Squad Automatic Weapon ) is a US 5.56mm NATO light machine gun adaptation of the Belgian FN Minimi.
	"rhs_weap_M320",							// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_weap_m4",							// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_weap_m4_grip",						// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_weap_m4_grip2",						// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_weap_m4_bipod",						// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_weap_m4_carryhandle",					// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_weap_m4_carryhandle_pmag",				// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_m4_m320",							// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_weap_m4a1",							// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_grip",						// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_grip2",						// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_bipod",						// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_carryhandle",				// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_carryhandle_grip",			// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_carryhandle_grip2",			// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_carryhandle_bipod",			// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_m4a1_m320",							// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_blockII",					// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_m4a1_blockII_grip2",				// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_weap_M590_5RD",						// The M590A1 is a shotgun used in the US Armed Forces.
	"rhs_weap_M590_8RD",						// The M590A1 is a shotgun used in the US Armed Forces.
	"rhs_weap_mk18",							// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_weap_mk18_grip2",						// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_weap_sr25",							// The SR-25 Enhanced Sniper Rifle is a is a semi-automatic special application sniper rifle derivative from the AR-15 ( like the M16A4 Rifle ).
	"rhs_weap_sr25_ec"						// The SR-25 Enhanced Sniper Rifle is a is a semi-automatic special application sniper rifle derivative from the AR-15 ( like the M16A4 Rifle ).
],true] call BIS_fnc_addVirtualWeaponCargo;

[_myBox,[
	"AGM_HandFlare_Green",
	"AGM_HandFlare_Red",
	"AGM_HandFlare_White",
	"AGM_HandFlare_Yellow",
	"AGM_M84",
	"AGM_M26_Clacker",
	"AGM_DeadManSwitch",
	"AGM_Clacker",
	"AGM_30Rnd_65x39_caseless_mag_Tracer_Dim",
	"AGM_30Rnd_65x39_caseless_mag_SD",
	"AGM_30Rnd_65x39_caseless_mag_AP",
	"AGM_30Rnd_65x39_caseless_green_mag_Tracer_Dim",
	"AGM_30Rnd_65x39_caseless_green_mag_SD",
	"AGM_30Rnd_65x39_caseless_green_mag_AP",
	"AGM_30Rnd_556x45_Stanag_Tracer_Dim",
	"AGM_30Rnd_556x45_Stanag_SD",
	"AGM_30Rnd_556x45_Stanag_AP",
	"AGM_20Rnd_762x51_Mag_Tracer",
	"AGM_20Rnd_762x51_Mag_Tracer_Dim",
	"AGM_20Rnd_762x51_Mag_SD",
	"AGM_20Rnd_762x51_Mag_AP",
	"AGM_20Rnd_762x51_Mag_LR",
	"rhsusf_20Rnd_762x51_m118_special_Mag",					// The M14 Enhanced Battle Rifle is a designated marksman evolution of the M14 rifle.
	"rhsusf_20Rnd_762x51_m993_Mag",							// The M14 Enhanced Battle Rifle is a designated marksman evolution of the M14 rifle.
	"rhs_mag_30Rnd_556x45_Mk262_Stanag",					// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_Mk318_Stanag",					// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag",					// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",			// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",			// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",		// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",		// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhs_mag_30Rnd_556x45_M200_Stanag",						// The M16A4 rifle is the last upgrade of the M16 which is the US 5.56mm NATO caliber military version of the AR-15.
	"rhsusf_5Rnd_300winmag_xm2010",							// The M2010 Enhanced Sniper Rifle is a derivation of the M24 sniper rifle.
	"rhsusf_100Rnd_762x51",								// The M240B is a US 7.62x51mm NATO medium machine gun that is fed from disintegrating belt.
	"rhsusf_100Rnd_556x45_soft_pouch",						// The M249 SAW ( Squad Automatic Weapon ) is a US 5.56mm NATO light machine gun adaptation of the Belgian FN Minimi.
	"rhsusf_100Rnd_556x45_M200_soft_pouch",					// The M249 SAW ( Squad Automatic Weapon ) is a US 5.56mm NATO light machine gun adaptation of the Belgian FN Minimi.
	"rhsusf_200Rnd_556x45_soft_pouch",						// The M249 SAW ( Squad Automatic Weapon ) is a US 5.56mm NATO light machine gun adaptation of the Belgian FN Minimi.
	"rhs_mag_M433_HEDP",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M4009",										// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_m576",										// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M585_white",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M661_green",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M662_red",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M713_red",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M714_white",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M715_green",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_M716_yellow",									// The M320 Grenade Launcher Module is a single-shot 40mm grenade launcher.
	"rhs_mag_30Rnd_556x45_Mk262_Stanag",					// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_Mk318_Stanag",					// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag",					// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",			// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",			// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",		// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",		// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_M200_Stanag",						// The M4 Carbine is a series of firearms that were developed from an early carbine version of the M16 rifle.
	"rhs_mag_30Rnd_556x45_Mk262_Stanag",					// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_Mk318_Stanag",					// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag",					// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",			// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",			// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",		// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",		// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhs_mag_30Rnd_556x45_M200_Stanag",						// The M4A1 Carbine is an upgrade of the M4 Carbine.
	"rhsusf_5Rnd_00Buck",									// The M590A1 is a shotgun used in the US Armed Forces.
	"rhsusf_8Rnd_00Buck",									// The M590A1 is a shotgun used in the US Armed Forces.
	"rhs_mag_30Rnd_556x45_Mk262_Stanag",					// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_Mk318_Stanag",					// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag",					// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",			// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",			// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",		// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",		// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhs_mag_30Rnd_556x45_M200_Stanag",						// The MK18 is a contracted carbine version of the M4 Carbine for CQB.
	"rhsusf_20Rnd_762x51_m118_special_Mag",					// The SR-25 Enhanced Sniper Rifle is a is a semi-automatic special application sniper rifle derivative from the AR-15 ( like the M16A4 Rifle ).
	"rhsusf_20Rnd_762x51_m993_Mag",							// The SR-25 Enhanced Sniper Rifle is a is a semi-automatic special application sniper rifle derivative from the AR-15 ( like the M16A4 Rifle ).
	"rhs_mag_m67",										// USAF Grenades & Mines
	"rhs_mag_m69",										// USAF Grenades & Mines
	"rhs_mag_mk84",										// USAF Grenades & Mines
	"rhs_mag_an_m8hc",									// USAF Grenades & Mines
	"rhs_mag_an_m14_th3",									// USAF Grenades & Mines
	"rhs_mag_m7a3_cs",									// USAF Grenades & Mines
	"rhs_mag_mk3a2",										// USAF Grenades & Mines
	"rhs_mag_m18_purple",									// USAF Grenades & Mines
	"rhs_mag_m18_green",									// USAF Grenades & Mines
	"rhs_mag_m18_red",									// USAF Grenades & Mines
	"rhs_mag_m18_yellow",									// USAF Grenades & Mines
	"rhs_mine_M19_ammo"									// USAF Grenades & Mines
],true] call BIS_fnc_addVirtualMagazineCargo;

[_myBox,[
	"AGM_NonSteerableParachute",
	"rhsusf_falconii",							// USAF Backpacks
	"rhsusf_assault_eagleaiii_ucp",					// USAF Backpacks
	"rhsusf_assault_eagleaiii_ocp",					// USAF Backpacks
	"rhsusf_assault_eagleaiii_coy",					// USAF Backpacks
	"rhsusf_assault_eagleaiii_ocp_engineer",			// USAF Backpacks
	"rhsusf_assault_eagleaiii_ocp_medic",			// USAF Backpacks
	"rhsusf_assault_eagleaiii_ocp_demo",			// USAF Backpacks
	"B_rhsusf_B_BACKPACK"							// USAF Backpacks
],true] call BIS_fnc_addVirtualBackpackCargo;




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Remove globally restricted gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-	

[_myBox,[
	"hgun_PDW2000_F",
	"arifle_Mk20C_F",
	"arifle_Mk20_GL_F",
	"arifle_Mk20_plain_F",
	"arifle_Mk20C_plain_F",
	"arifle_Mk20_GL_plain_F",
	"srifle_EBR_F",
	"srifle_GM6_F",
	"launch_I_Titan_F",
	"launch_I_Titan_short_F",
	"launch_Titan_F",
	"launch_Titan_short_F"
],true] call BIS_fnc_removeVirtualWeaponCargo;	
	
	
[_myBox,[
	"B_HMG_01_support_F",
	"B_HMG_01_support_high_F",
	"B_Mortar_01_support_F",
	"I_HMG_01_weapon_F",
	"I_GMG_01_weapon_F",
	"I_HMG_01_high_weapon_F",
	"I_GMG_01_high_weapon_F",
	"I_HMG_01_support_F",
	"I_HMG_01_support_high_F",
	"I_Mortar_01_weapon_F",
	"I_Mortar_01_support_F",
	"O_HMG_01_support_F",
	"O_GMG_01_support_high_F",
	"O_Mortar_01_support_F",
	"B_UAV_01_backpack_F",
	"O_UAV_01_backpack_F",
	"I_UAV_01_backpack_F"
],true] call BIS_fnc_removeVirtualBackpackCargo;


[_myBox,[
	"NLAW_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA",
	"RPG32_F",
	"RPG32_HE_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA"
],true] call BIS_fnc_removeVirtualMagazineCargo;
	

[_myBox,[
	"optic_NVS",
	"optic_Nightstalker",
	"optic_tws",
	"optic_tws_mg",
	"U_I_CombatUniform",
	"U_I_CombatUniform_shortsleeve",
	"U_I_CombatUniform_tshirt",
	"U_I_OfficerUniform",
	"U_I_GhillieSuit",
	"U_I_HeliPilotCoveralls",
	"U_I_pilotCoveralls",
	"U_I_Wetsuit",
	"U_IG_Guerilla1_1",
	"U_IG_Guerilla2_1",
	"U_IG_Guerilla2_2",
	"U_IG_Guerilla2_3",
	"U_IG_Guerilla3_1",
	"U_IG_Guerilla3_2",
	"U_IG_leader",
	"U_BG_Guerilla3_2",
	"U_OG_Guerilla3_2",
	"U_C_Poloshirt_blue",
	"U_C_Poloshirt_burgundy",
	"U_C_Poloshirt_stripped",
	"U_C_Poloshirt_tricolour",
	"U_C_Poloshirt_salmon",
	"U_C_Poloshirt_redwhite",
	"U_C_Commoner1_1",
	"U_C_Commoner1_2",
	"U_C_Commoner1_3",
	"U_C_Poor_1",
	"U_C_Poor_2",
	"U_C_Scavenger_1",
	"U_C_Scavenger_2",
	"U_C_Farmer",
	"U_C_Fisherman",
	"U_C_WorkerOveralls",
	"U_C_FishermanOveralls",
	"U_C_WorkerCoveralls",
	"U_C_HunterBody_grn",
	"U_C_HunterBody_brn",
	"U_C_Commoner2_1",
	"U_C_Commoner2_2",
	"U_C_Commoner2_3",
	"U_C_PriestBody",
	"U_C_Poor_shorts_1",
	"U_C_Poor_shorts_2",
	"U_C_Commoner_shorts",
	"U_C_ShirtSurfer_shorts",
	"U_C_TeeSurfer_shorts_1",
	"U_C_TeeSurfer_shorts_2",
	"U_NikosBody",
	"U_MillerBody",
	"U_KerryBody",
	"U_OrestesBody",
	"U_AttisBody",
	"U_AntigonaBody",
	"U_IG_Menelaos",
	"U_C_Novak",
	"U_OI_Scientist",
	"V_PlateCarrierIA1_dgtl",
	"V_PlateCarrierIA2_dgtl",
	"V_PlateCarrierIAGL_dgtl",
	"V_RebreatherIA",
	"V_TacVest_oli",
	"V_TacVest_camo",
	"V_TacVest_blk_POLICE",
	"V_TacVestIR_blk",
	"V_TacVestCamo_khk",
	"V_BandollierB_cbr",
	"V_BandollierB_oli",
	"H_HelmetIA",
	"H_HelmetIA_net",
	"H_HelmetIA_camo",
	"H_HelmetCrew_I",
	"H_CrewHelmetHeli_I",
	"H_PilotHelmetHeli_I",
	"H_PilotHelmetFighter_I",
	"H_Booniehat_dgtl",
	"H_Booniehat_indp",
	"H_MilCap_dgtl",
	"H_Cap_grn",
	"H_Cap_red",
	"H_Cap_blu",
	"H_Cap_tan",
	"H_Cap_blk",
	"H_Cap_grn_BI",
	"H_Cap_blk_Raven",
	"H_Cap_blk_ION",
	"H_Cap_blk_CMMG",
	"H_MilCap_rucamo",
	"H_MilCap_gry",
	"H_MilCap_blue",
	"H_Booniehat_grn",
	"H_Booniehat_tan",
	"H_Booniehat_dirty",
	"H_StrawHat",
	"H_StrawHat_dark",
	"H_Hat_blue",
	"H_Hat_brown",
	"H_Hat_camo",
	"H_Hat_grey",
	"H_Hat_checker",
	"H_Hat_tan",
	"H_Bandanna_surfer",
	"H_Bandanna_cbr",
	"H_Bandanna_sgg",
	"H_Bandanna_gry",
	"H_Bandanna_camo",
	"H_TurbanO_blk",
	"H_Shemag_khk",
	"H_Shemag_tan",
	"H_ShemagOpen_khk",
	"H_ShemagOpen_tan",
	"H_Beret_blk",
	"H_Beret_blk_POLICE",
	"H_Beret_red",
	"H_Beret_grn",
	"H_Watchcap_khk",
	"H_Watchcap_sgg",
	"H_BandMask_blk",
	"H_BandMask_khk",
	"H_BandMask_reaper",
	"H_BandMask_demon",
	"H_Shemag_olive_hs",
	"H_Cap_oli_hs",
	"I_UavTerminal",
	"H_Booniehat_khk_hs"
],true] call BIS_fnc_removeVirtualItemCargo;

diag_log format ["%1: Transport & Ambush: WEST Virtual Arsenal initiated for: %2:", time, _myBox];
