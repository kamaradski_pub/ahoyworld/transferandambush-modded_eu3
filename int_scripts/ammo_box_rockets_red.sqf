//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			ammo_box_rockets_red.sqf
// Author:		Kamaradski 2014
// Contributers:	Jester
//
// Crate loading script with restricted ammo for "Transport & Ambush"
// This script will respawn the ammobox content every 20 min
// _K = [this] execVM "int_scripts\ammo_box_rockets_red.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (!isServer) exitWith {};

sleep 7; // allow other modules to load first

_myBox = _this select 0;
_myBox allowDamage false; 

while {true} do {

	diag_log format ["%1: Transport & Ambush: (re)Spawned restricted ammo RED-rockets:", time];
	clearWeaponCargoGlobal _myBox;
	clearMagazineCargoGlobal _myBox;
	clearBackpackCargoGlobal _myBox;
	clearItemCargoGlobal  _myBox;
	sleep 2;
	_myBox addMagazineCargoGlobal ["RPG32_F",2];
	_myBox addMagazineCargoGlobal ["RPG32_HE_F",2];
	_myBox addMagazineCargoGlobal ["Titan_AT",2];
	_myBox addMagazineCargoGlobal ["Titan_AP",2];
	_myBox addMagazineCargoGlobal ["Titan_AA",2];
	_myBox addMagazineCargoGlobal ["rhs_mag_9k38_rocket",2];		// The 9K38 Igla is a man-portable surface-to-air missile guided by infrared.
	_myBox addMagazineCargoGlobal ["rhs_mine_pmn2",2];				// Mines
	_myBox addMagazineCargoGlobal ["rhs_mine_tm62m",2];			// Mines
	_myBox addMagazineCargoGlobal ["rhs_rpg26_mag",2];				// The RPG-26 is a man-portable, anti-tank rocket-propelled grenade launcher.
	_myBox addMagazineCargoGlobal ["rhs_rpg7_PG7VL_mag",2];		// The RPG-7 is a man-portable, anti-tank rocket-propelled grenade launcher.
	_myBox addMagazineCargoGlobal ["rhs_rpg7_PG7VR_mag",2];		// The RPG-7 is a man-portable, anti-tank rocket-propelled grenade launcher.
	_myBox addMagazineCargoGlobal ["rhs_rpg7_OG7V_mag",2];			// The RPG-7 is a man-portable, anti-tank rocket-propelled grenade launcher.
	_myBox addMagazineCargoGlobal ["rhs_rpg7_TBG7V_mag",2];		// The RPG-7 is a man-portable, anti-tank rocket-propelled grenade launcher.
	_myBox addMagazineCargoGlobal ["rhs_rshg2_mag",2];				// The RShG-2 is a variant of the RPG-26 armed with a thermobaric warhead.
	sleep 1200; // Sleep 20 min before emptying the box, and respawn it's content
};
