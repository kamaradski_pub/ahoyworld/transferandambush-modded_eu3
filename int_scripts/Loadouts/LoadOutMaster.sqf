/*
| Author: 
|
|	Pfc.Christiansen, Stuffedsheep
|_____
|
|   Description: LoadoutMaster
|   Usage InitPlayerLocal.sqf: waitUntil {!isNull player};
|                              null = [] execVM "int_scripts\Loadouts\LoadoutMaster.sqf";
|
|
|		Usage Init.sqf           : waitUntil {!isNull player};
|											if !(isServer) then {
|																		null = [] execVM "int_scripts\Loadouts\LoadoutMaster.sqf"; 		//Standard Loadouts
|																	};
|	
|	Created: 17/01-2015
|	Last modified: 01/03-2015
|	Made For AhoyWorld Internal Use.
*/

/* T&A Checklist:
WEST:
B_officer_F

B_Soldier_SL_F
B_recon_medic_F
B_recon_F
B_recon_F
B_recon_LAT_F

B_Soldier_SL_F
B_medic_F
rhsusf_army_ocp_machinegunner
rhsusf_army_ocp_machinegunnera
B_Soldier_GL_F

B_Soldier_SL_F
B_medic_F
B_soldier_AT_F
B_Soldier_GL_F
B_soldier_M_F

B_Soldier_SL_F
B_Helipilot_F
B_helicrew_F
B_soldier_repair_F
B_soldier_exp_F

EAST:
O_officer_F

O_Soldier_SL_F
O_recon_medic_F
O_recon_F
O_recon_F
O_recon_M_F

O_Soldier_SL_F
O_medic_F
rhs_msv_machinegunner
rhs_msv_machinegunner_assistant
O_Soldier_GL_F

O_Soldier_SL_F
O_medic_F
O_Soldier_GL_F
O_Soldier_AT_F
O_soldier_M_F

O_Soldier_SL_F
O_helipilot_F
O_helicrew_F
O_soldier_exp_F
O_soldier_repair_F
*/

/*================================================================================================*/
/*============================================ Blufor ============================================*/
/*================================================================================================*/


/*============================================= Lead =============================================*/
if (playerside == west) then {

// Platoon Leader
	if (typeOf player == "B_officer_F") then {
			null = [] execVM "int_scripts\Loadouts\West\1.Platoon_Leader.sqf";
	};	

// Squad Leader
	if (typeOf player == "B_Soldier_SL_F") then {
			null = [] execVM "int_scripts\Loadouts\West\2.SquadLeader.sqf";
	};	

// Team Leader
	if (typeOf player == "B_Soldier_TL_F") then {
			null = [] execVM "int_scripts\Loadouts\West\3.TeamLeader.sqf";
	};

// FAC
	if (typeOf player == "B_recon_JTAC_F") then {
			null = [] execVM "int_scripts\Loadouts\West\4.FAC.sqf";
	};

/*=========================================== Riflemen ===========================================*/

// Rifleman
	if (typeOf player == "B_Soldier_F") then {
			null = [] execVM "int_scripts\Loadouts\West\5.Rifleman.sqf";
	};
	if (typeOf player == "B_recon_F") then {
			null = [] execVM "int_scripts\Loadouts\West\5.Rifleman.sqf";
	};

// Grenadier	
	if (typeOf player == "B_Soldier_GL_F") then {
			null = [] execVM "int_scripts\Loadouts\West\6.Grenadier.sqf";
	};

// Medic
	if (typeOf player == "B_medic_F") then {
			null = [] execVM "int_scripts\Loadouts\West\7.Medic.sqf";
	};
	if (typeOf player == "B_recon_medic_F") then {
			null = [] execVM "int_scripts\Loadouts\West\7.Medic.sqf";
	};

// Marksman
	if (typeOf player == "B_soldier_M_F") then	{
			null = [] execVM "int_scripts\Loadouts\West\8.Marksman.sqf";
	};

// Explosive Specialist
	if (typeOf player == "B_soldier_exp_F") then {
			null = [] execVM "int_scripts\Loadouts\West\9.Explosive_Spec.sqf";
	};

// Automatic Rifleman
	if (typeOf player == "B_soldier_AR_F") then {
			null = [] execVM "int_scripts\Loadouts\West\10.AutoRifleman.sqf";
	};

// Assistant Automatic Rifleman
	if (typeOf player == "B_soldier_AAR_F") then {
			null = [] execVM "int_scripts\Loadouts\West\11.AssistAR.sqf";
	};

/*======================================= Specialist Teams =======================================*/

// Medium Machnine Gunner
	if (typeOf player == "rhsusf_army_ocp_machinegunner") then {
			null = [] execVM "int_scripts\Loadouts\West\12.MMG.sqf";
	};

// Assistant Medium Machnine Gunner
	if (typeOf player == "rhsusf_army_ocp_machinegunnera") then {
			null = [] execVM "int_scripts\Loadouts\West\13.AssistMMG.sqf";
	};

// Anti-Tank Specialist	
	if (typeOf player == "B_soldier_AT_F") then {
			null = [] execVM "int_scripts\Loadouts\West\14.MissleSpecAT.sqf";
	};
	if (typeOf player == "B_recon_LAT_F") then {
			null = [] execVM "int_scripts\Loadouts\West\14.MissleSpecAT.sqf";
	};

// Assistant Anti-Tank Specialist
	if (typeOf player == "B_soldier_AAT_F") then {
			null = [] execVM "int_scripts\Loadouts\West\15.AssistMissleSpecAT.sqf";
	};

// Missile AA 	
	if (typeOf player == "B_soldier_AA_F") then {
			null = [] execVM "int_scripts\Loadouts\West\16.MissleSpecAA.sqf";
	};

// Missile AA asst
	if (typeOf player == "B_soldier_AAA_F") then {
			null = [] execVM "int_scripts\Loadouts\West\17.AssistMissleSpecAA.sqf";
	};

// Repair Spec
	if (typeOf player == "B_soldier_repair_F") then {
			null = [] execVM "int_scripts\Loadouts\West\22.Repair_Spec.sqf";
	};

/*======================================= Pilot's and Crew =======================================*/

// Pilot
	if (typeOf player == "B_Pilot_F") then {
			null = [] execVM "int_scripts\Loadouts\West\18.Pilot.sqf";
	};
	if (typeOf player == "B_Helipilot_F") then {
			null = [] execVM "int_scripts\Loadouts\West\18.Pilot.sqf";
	};

// Co-Pilot
	if (typeOf player == "B_helicrew_F") then {
			null = [] execVM "int_scripts\Loadouts\West\19.Co-Pilot.sqf";
	};

// Crew Commander
	if (typeOf player == "rhsusf_army_ocp_combatcrewman") then {
			null = [] execVM "int_scripts\Loadouts\West\20.CrewCommander.sqf";
	};

// Crew
	if (typeOf player == "B_crew_F") then {
			null = [] execVM "int_scripts\Loadouts\West\21.Crew.sqf";
	};
};

/*================================================================================================*/
/*============================================= Opfor ============================================*/
/*================================================================================================*/

/*============================================= Lead =============================================*/

if (playerside == east) then {

// Platoon Leader
	if (typeOf player == "O_officer_F") then {
			null = [] execVM "int_scripts\Loadouts\East\1.Platoon_Leader.sqf";
	};	

// Squad Leader
	if (typeOf player == "O_Soldier_SL_F") then {
			null = [] execVM "int_scripts\Loadouts\East\2.SquadLeader.sqf";
	};	

// Team Leader
	if (typeOf player == "O_Soldier_TL_F") then {
			null = [] execVM "int_scripts\Loadouts\East\3.TeamLeader.sqf";
	};

// FAC
	if (typeOf player == "O_recon_JTAC_F") then {
			null = [] execVM "int_scripts\Loadouts\East\4.FAC.sqf";
	};

/*=========================================== Riflemen ===========================================*/

// Rifleman
	if (typeOf player == "O_recon_F") then {
			null = [] execVM "int_scripts\Loadouts\East\5.Rifleman.sqf";
	};
	if (typeOf player == "O_recon_F") then {
			null = [] execVM "int_scripts\Loadouts\East\5.Rifleman.sqf";
	};

// Grenadier	
	if (typeOf player == "O_Soldier_GL_F") then {
			null = [] execVM "int_scripts\Loadouts\East\6.Grenadier.sqf";
	};

// Medic
	if (typeOf player == "O_medic_F") then {
			null = [] execVM "int_scripts\Loadouts\East\7.Medic.sqf";
	};
	if (typeOf player == "O_recon_medic_F") then {
			null = [] execVM "int_scripts\Loadouts\East\7.Medic.sqf";
	};

// Marksman
	if (typeOf player == "O_soldier_M_F") then	{
			null = [] execVM "int_scripts\Loadouts\East\8.Marksman.sqf";
	};
	if (typeOf player == "O_recon_M_F") then {
			null = [] execVM "int_scripts\Loadouts\East\8.Marksman.sqf";
	};

// Explosive Specialist
	if (typeOf player == "O_soldier_exp_F") then {
			null = [] execVM "int_scripts\Loadouts\East\9.Explosive_Spec.sqf";
	};

// Automatic Rifleman
	if (typeOf player == "O_Soldier_AR_F") then {
			null = [] execVM "int_scripts\Loadouts\East\10.AutoRifleman.sqf";
	};

// Assistant Automatic Rifleman
	if (typeOf player == "O_Soldier_AAR_F") then {
			null = [] execVM "int_scripts\Loadouts\East\11.AssistAR.sqf";
	};

/*======================================= Specialist Teams =======================================*/

// Medium Machnine Gunner
	if (typeOf player == "rhs_msv_machinegunner") then	{
			null = [] execVM "int_scripts\Loadouts\East\12.MMG.sqf";
	};

// Assistant Medium Machnine Gunner
	if (typeOf player == "rhs_msv_machinegunner_assistant") then	{
			null = [] execVM "int_scripts\Loadouts\East\13.AssistMMG.sqf";
	};

// Anti-Tank Specialist	
	if (typeOf player == "O_soldier_AT_F") then {
			null = [] execVM "int_scripts\Loadouts\East\14.MissleSpecAT.sqf";
	};
	if (typeOf player == "O_recon_LAT_F") then	{
			null = [] execVM "int_scripts\Loadouts\East\14.MissleSpecAT.sqf";
	};

// Assistant Anti-Tank Specialist
	if (typeOf player == "O_soldier_AAT_F") then {
			null = [] execVM "int_scripts\Loadouts\East\15.AssistMissleSpecAT.sqf";
	};

// Missile AA 	
	if (typeOf player == "O_soldier_AA_F") then {
			null = [] execVM "int_scripts\Loadouts\East\16.MissleSpecAA.sqf";
	};

// Missile AA asst
	if (typeOf player == "O_soldier_AAA_F") then {
			null = [] execVM "int_scripts\Loadouts\East\17.AssistMissleSpecAA.sqf";
	};

// Repair Spec
	if (typeOf player == "O_soldier_repair_F") then {
			null = [] execVM "int_scripts\Loadouts\East\22.Explosive_Spec.sqf";
	};

/*======================================= Pilot's and Crew =======================================*/

// Pilot
	if (typeOf player == "O_Pilot_F") then {
			null = [] execVM "int_scripts\Loadouts\East\18.Pilot.sqf";
	};
	if (typeOf player == "O_helipilot_F") then	{
			null = [] execVM "int_scripts\Loadouts\East\18.Pilot.sqf";
	};

// Co-Pilot
	if (typeOf player == "O_helicrew_F") then	{
			null = [] execVM "int_scripts\Loadouts\East\19.Co-Pilot.sqf";
	};

// Crew Commander
	if (typeOf player == "rhs_msv_crew_commander") then	{
			null = [] execVM "int_scripts\Loadouts\East\20.CrewCommander.sqf";
	};

// Crew
	if (typeOf player == "O_crew_F") then	{
			null = [] execVM "int_scripts\Loadouts\East\21.Crew.sqf";
	};
};

// EOF