removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_cu_ocp_patchless";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "AGM_IR_Strobe_Item";
for "_i" from 1 to 2 do {player addItemToUniform "RH_15Rnd_45cal_fnp";};
player addVest "rhsusf_iotv_ocp_Medic";
for "_i" from 1 to 7 do {player addItemToVest "rhs_mag_30Rnd_556x45_Mk318_Stanag";};
player addItemToVest "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red";
for "_i" from 1 to 2 do {player addItemToVest "HandGrenade";};
player addItemToVest "SmokeShell";
player addItemToVest "SmokeShellGreen";
player addItemToVest "SmokeShellBlue";
player addItemToVest "AGM_HandFlare_White";
player addBackpack "rhsusf_assault_eagleaiii_ocp";
for "_i" from 1 to 25 do {player addItemToBackpack "AGM_Bandage";};
for "_i" from 1 to 16 do {player addItemToBackpack "AGM_Morphine";};
for "_i" from 1 to 8 do {player addItemToBackpack "AGM_Epipen";};
for "_i" from 1 to 6 do {player addItemToBackpack "AGM_Bloodbag";};
player addHeadgear "rhsusf_ach_helmet_headset_ocp";

player addWeapon "rhs_weap_m4_carryhandle";
player addPrimaryWeaponItem "rhsusf_acc_anpeq15";
player addWeapon "RH_fnp45t";
player addHandgunItem "RH_X300";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "tf_microdagr";
player linkItem "tf_rf7800str";
player linkItem "rhsusf_ANPVS_14";