removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_df15";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "AGM_MapTools";
for "_i" from 1 to 2 do {player addItemToUniform "RH_18Rnd_9x19_VP";};
player addVest "V_TacVest_blk";
for "_i" from 1 to 4 do {player addItemToVest "hlc_30Rnd_545x39_B_AK";};
for "_i" from 1 to 2 do {player addItemToVest "rhs_mag_rdg2_white";};
player addItemToVest "rhs_mag_rdg2_black";
player addItemToVest "rhs_mag_nspn_red";
player addItemToVest "O_IR_Grenade";
player addBackpack "B_AssaultPack_blk";
player addItemToBackpack "ToolKit";
player addHeadgear "rhs_zsh7a_mike";

player addWeapon "hlc_rifle_aku12";
player addWeapon "RH_vp70";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_fadak";
player linkItem "ItemGPS";