removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "AGM_ItemKestrel";
player addItemToUniform "AGM_MapTools";
player addItemToUniform "rhs_mag_rgd5";
player addItemToUniform "rhs_mag_rdg2_white";
player addVest "rhs_6b23_ML_sniper";
for "_i" from 1 to 8 do {player addItemToVest "rhs_10Rnd_762x54mmR_7N1";};
player addItemToVest "rhs_mag_rgd5";
player addHeadgear "rhs_Booniehat_flora";

player addWeapon "rhs_weap_svdp_npz";
player addPrimaryWeaponItem "FHQ_optic_LeupoldERT";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_pnr1000a";