removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "rhs_mag_rgd5";
player addItemToUniform "rhs_mag_rdg2_white";
player addVest "rhs_6b23_ML_6sh92";
for "_i" from 1 to 5 do {player addItemToVest "rhs_45Rnd_545X39_AK";};
for "_i" from 1 to 2 do {player addItemToVest "RH_8Rnd_9x18_Mak";};
player addItemToVest "rhs_mag_rgd5";
player addBackpack "rhs_sidor";
for "_i" from 1 to 10 do {player addItemToBackpack "rhs_45Rnd_545X39_AK";};
player addHeadgear "rhs_6b27m_ml_bala";

player addWeapon "rhs_weap_ak74m_plummag_npz";
player addWeapon "RH_mak";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_pnr1000a";