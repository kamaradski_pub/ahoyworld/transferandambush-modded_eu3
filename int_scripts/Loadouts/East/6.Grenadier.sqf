removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {player addItemToUniform "rhs_mag_rgd5";};
player addVest "rhs_6b23_ML_6sh92_vog";
player addItemToVest "rhs_mag_rgd5";
for "_i" from 1 to 6 do {player addItemToVest "rhs_30Rnd_545x39_AK";};
for "_i" from 1 to 6 do {player addItemToVest "rhs_VOG25";};
player addHeadgear "rhs_6b27m_ML_ess_bala";

player addWeapon "rhs_weap_ak74m_gp25_npz";
player addPrimaryWeaponItem "rhs_acc_dtk";

player addVest "rhs_6b23_ML_6sh92_vog";
player addItemToVest "rhs_30Rnd_545x39_AK";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_pnr1000a";